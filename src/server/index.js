
function csvjson() {                  // function to the csv data files into json data files

    const csvtojson = require('csvtojson')
    
    const fs = require('fs')
    
    const fp = '../data/matches.csv'
    
    csvtojson()
    .fromFile(fp)
    .then((json) => {
    
        fs.writeFileSync('../data/matches.json',JSON.stringify(json),'utf-8',(err) => {
            if (err) console.log(err)
        })
    })
    
    }
        
//csvjson()                                                        uncomment this function call to convert the data into json format
    

let output_function = require('./ipl.js')                          // importing the functions

let matches = require('../data/matches.json')                      // importing the matches data

let deliveries = require('../data/deliveries.json')                 // importing the deliveries data


function writefile (fp,fn) {                                       // function to get the output

    const fs = require('fs')

    fs.writeFile(fp,JSON.stringify(fn), err => {

        if (err) console.log(err)
    })
}


const match_output = output_function.economical_bowlers(deliveries)           // function to get the output of corresponding question

const file_path = '../public/output/10_economical_bowlers_2015.json'            // path to store the output file

 writefile(file_path,match_output)                 





