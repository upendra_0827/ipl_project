
//No of matches 

function matches(data) {                                                // declaring the function 
 
    const no_of_matches = {}                                        // object to store the result

    for (let match=0 ; match<data.length ; match++) {                   // loop to iterate all the matches 
	
	    if (data[match]['season'] in no_of_matches) {               // checking if the season is all ready entered in the object
		    no_of_matches[data[match]['season']] = no_of_matches[data[match]['season']]+1     // increasing the count of season if present
	    } else {
		    no_of_matches[data[match]['season']] = 1                 // entry of new season with count 1
	    }
    } return no_of_matches                                         // return the matches object
}


// Number of matches won per team per year in IPL.


function win_years(data) {                                                 // function declaration 

    const wins_per_team = {}                                              // object to store final result

    for (let row=0 ; row<data.length ; row++) {                               // iterating through each match to record the seasons with an empty object to store winning data of that year

        wins_per_team[data[row]['season']] = {}                              // entry of season with a blank object to store corresponding data
    }

    for (let match=0 ; match<data.length ; match++) {                        // loop to iterate each match

        if (data[match]['winner'] in wins_per_team[data[match]['season']]) {     // checking if the team is already present in the corresponding season 
            wins_per_team[data[match]['season']][data[match]['winner']] += 1     // increasing winning count by 1 if the team is present in the corresponding year

        } else {
            wins_per_team[data[match]['season']][data[match]['winner']] = 1        // entry of new team with winning count 1
        }   
    } return wins_per_team                                                  // returning the result                                                        
}


// Extra runs conceded per team in the year 2016


function extra_runs (data) {                                      // function declaration

    ex_runs = {}                                               // object to store final result

    for (let match=0 ; match<data.length-1 ; match++) {            // loop to iterate through each match
    
        if (data[match]['match_id'] > 576) {                      // to select the 2016's matches with their corresponding id's

        if (data[match]['bowling_team'] in ex_runs) {             // checking if the team is already present in the object
            ex_runs[data[match]['bowling_team']] += parseInt(data[match]['extra_runs'])    // increasing the count of conceeded runs of that team
        } else {
            ex_runs[data[match]['bowling_team']] = parseInt(data[match]['extra_runs'])    // entry of new team with respective runs conceeded 
        }}
    } return ex_runs                                                // returning the result
}


// Top 10 economical bowlers in the year 2015


function economical_bowlers(data) {                              // function declaration

    let bowler_overs = {}                                         // to store the number of balls bowled by bowler 
    let conceeded_runs = {}                                       // to store conceeded runs
    let bowler_economy = {}                                      // to store bowler's economy with their names
    let economy_array = []                                        // to store the values of bowler's economy
    let top_10_bowlers = []                                       // to store top 10 bowlers


    for (match=0; match<data.length ; match++) {                 // loop to iterate matches to calculate balls bowled 

        if (data[match]['match_id']>517 && data[match]['match_id'] < 577) {      // to select 2015's matches
    
        if (data[match]['bowler'] in bowler_overs) {                       // to check if the bowler is already present

            bowler_overs[data[match]['bowler']] += 1                     // increaing the no. of balls bowled by 1

        } else {

            bowler_overs[data[match]['bowler']] = 1                      // listing new bowler with record of one ball bowled
        }}
   
    }

    for (let match=0 ; match<data.length ; match++) {                    // loop to calculate conceeded runs 

        if (data[match]['match_id']>517 && data[match]['match_id']<577) {     // to select 2015's matches

        if (data[match]['bowler'] in conceeded_runs) {                     // to check if the bowler's data been already recorded
            conceeded_runs[data[match]['bowler']] += parseInt(data[match]['total_runs'])      // increasing the bowler's conceeded runs count by 1

        } else {

            conceeded_runs[data[match]['bowler']] = parseInt(data[match]['total_runs'])   // recording new bowler with corresponding runs conceeded
        }}
    }

    for (let key of Object.keys(bowler_overs)) {                             // iterating over keys of object
        let val = conceeded_runs[key]/parseInt(bowler_overs[key]/6)            // calculating economy of particular bowler
    
        economy_array.push(val)                                              // pushing the value into economy_array for later usage
        bowler_economy[val] = key                                              // pushing economy and bowler into bowler_economy object to access bowler with their economy
    }

    economy_array.sort(function(a, b){return a-b})                       // sorting the economy values in ascending order
 
    for (let element=0 ; element<10 ; element++) {                           // loop to iterate economy_array
        top_10_bowlers.push(bowler_economy[economy_array[element]])         // accessing the bowler's name with the first 10 economy values they are ordered in ascending manner
    } return top_10_bowlers                                             // returning top 10 bowlers array
}

module.exports = {matches,win_years,extra_runs,economical_bowlers}         // exporting the functions 



